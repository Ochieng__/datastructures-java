package com.company;

import com.company.linkedlists.MLinkedList;
import com.company.linkedlists.MNode;
import com.company.realworld.linkedlists.Rumour;
import com.company.realworld.linkedlists.RumourBearer;

public class Main {

    public static void main(String[] args) {
        MLinkedList rumourBearers = new MLinkedList<RumourBearer>();
        MNode rumour = new MNode(new Rumour());
        rumourBearers.addFront(rumour);
        rumourBearers.printList();
    }
}
