package com.company.realworld.linkedlists;

import java.time.LocalDateTime;

public class Rumour {
    LocalDateTime dayTold;
    public Rumour(){this.dayTold=LocalDateTime.now();}

    @Override
    public String toString() {
        return "Rumour{" +
                "dayTold=" + dayTold.toString() +
                '}';
    }
}
