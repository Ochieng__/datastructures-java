package com.company.linkedlists;

public class MNode <T> {
    T data;
    MNode <T> next;
    public MNode(T d) {data = d;}

    @Override
    public String toString() {
        return "MNode{" +
                "data=" + data +
                ", next=" + next +
                '}';
    }
}
