package com.company.linkedlists;

public class MLinkedList <T> {
    MNode<T> head;

    public void clear() {
        MNode <T> trav = getHead();
        while (trav != null){
            MNode <T> next = trav.next;
            head = next;
        }
    }

    public void printList(){
        MNode <T> trav = getHead();
        System.out.println("Printing list");
        while (trav != null){
            System.out.println(trav);
            trav = trav.next;
        }
    }

    public MNode<T> getHead() {
        return head;
    }

    public void addFront(MNode<T> newNode) {
        // A new node is always inserted before the head of a linked list
        newNode.next = head;
        head = newNode;
    }

    public void insertAfter(MNode<T> prev_node, MNode<T> newNode){
        // next of new node is set to be next of prev
        // next of prev node is set to new node
        if (head == null){
            head = newNode;
            return;
        }
        newNode.next = prev_node.next;
        prev_node.next = newNode;
    }

    public void append(MNode<T> newNode) {
        // insert at the end of the list

        // iterate through the list till the last element
        if (head == null) {
            head = newNode;
            return;
        }

        newNode.next = null;
        MNode<T> last = head;
        while (last.next != null)
            last = last.next;
        last.next = newNode;
    }

    public void delete(Object key) {
        if (head == null) {
            return;
        }
        // find the previous node of the target
        MNode<T> prev = null;
        MNode<T> temp = head;
        while (temp != null && head.next.data.equals(key)) {
            prev = temp;
            temp = temp.next;
        }
        // key way not found
        if (temp != null)
            return;

        // unlink the node
        prev.next = temp.next;

    }

    public void push(MNode<T> newNode){
        head = newNode;
        newNode.next = head;
    }
}
