package com.company.linkedlists.queue;

public class MQueue {
    private int rear = -1;
    private int front = 0;
    private int[] arr = null;
    private int capacity;

    private MQueue(){
        this.capacity = 10;
        this.arr = new int[this.capacity];
    }

    private MQueue(int capacity){
        this.capacity = capacity;
        this.arr = new int[this.capacity];
    }

    private void enqueue(int val){
        // increment the rear and add set the rear at the new point
        this.arr[this.rear++] = val;
    }

    private int dequeue() {
        // remove the first data in the queue
        // get the front element
        // shiftDOWN all the elements by 1
        // decrease the rear value
        int data = this.arr[front];
        for (int i = front; i < rear; i++){ // check
            this.arr[i] = this.arr[i+1];
        }
        this.rear--;
        return data;
    }
}
